import socket
import machine
import os
import network
import time
import ujson
import ubinascii

from machine import SD
from network import WLAN
from network import LTE

class Data:

    def __init__(self,config):
        self.config = config

        #-------Connect to wifi-------
        wifi = config["wifi"]
        wlan = WLAN(mode=WLAN.STA)
        if (wifi["ns"] == "WPA2"):
            print("Connecting using WPA2 !")
            wlan.connect(wifi["ssid"], auth=(WLAN.WPA2, wifi["password"]), timeout=10000)

        elif (wifi["ns"] == "WPA2_ENT"):
            print("Connecting using WPA2 ENTREPRISE !")
            wlan.connect(wifi["ssid"], auth=(WLAN.WPA2_ENT, wifi["user"], wifi["password"]), identity = wifi["user"], timeout=10000)

        machine.idle()
        print("searching wifi...")
        time.sleep(4)

        if wlan.isconnected():

            print("Wifi Connected")

        #-------Connect to LTE--------
        elif not wlan.isconnected():
            wlan.disconnect()
            print("Wifi not Connected")
            machine.idle()
            print("try to connect LTE....")
            
            lte = LTE()
            lte.attach(band=20, apn="TM")
            print("attaching..",end='')
            while not lte.isattached():
                time.sleep(1)

                print('.',end='')
                print(lte.send_at_cmd('AT!="fsm"')) # get the System FSM
            print("attached!")

            lte.connect()
            print("connecting [##",end='')
            while not lte.isconnected():
                time.sleep(1)
                print('#',end='')

                print(lte.send_at_cmd('AT!="fsm"'))
            print("] connected!")
        else:
            print("no WIFI, no LTE")

    # user specified callback function
    
    def customCallback(client, userdata, message):
        print("Received a new message: ")
        print(message.payload)
        print("from topic: ")
        print(message.topic)
        print("--------------\n\n") 


    def can_callback(can_o):
        msg = can_o.recv()
        print('CAN Rx ID: ', hex(msg.id), ', Data:', ubinascii.hexlify(msg.data))
