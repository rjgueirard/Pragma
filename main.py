#!/usr/bin/env python
#
# Copyright (c) 2020, Pycom Limited.
#
# This software is licensed under the GNU GPL version 3 or any
# later version, with permitted additional terms. For more information
# see the Pycom Licence v1.0 document supplied with this file, or
# available at https://www.pycom.io/opensource/licensing

import machine
import math
import network
import os
import time
import utime
import gc
import socket
import ujson
import json
import pycom
# import AWSIoTPythonSDK.MQTTLib

from machine import RTC
from machine import SD
from L76GNSS import L76GNSS
from pycoproc_1 import Pycoproc
from LIS2HH12 import LIS2HH12
from Data import Data
from machine import CAN
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient


#read SD card
sd = SD()
os.mount(sd, '/sd')

gc.enable()

f = open('/sd/config_miarantsoa.json', 'r') #Read the config in the sd card
config = ujson.loads(f.read())

#data = Data(config, mqtt_callback)
data = Data(config)

# setup rtc
rtc = machine.RTC()
rtc.ntp_sync("pool.ntp.org")
utime.sleep_ms(750)
print('\nRTC Set from NTP to UTC:', rtc.now())
utime.timezone(7200)
print('Adjusted from UTC to EST timezone', utime.localtime(), '\n')

py = Pycoproc(Pycoproc.PYTRACK)
l76 = L76GNSS(py, timeout=30) #Setup GPS 

# Display the list of file on SD card
# print('files ', os.listdir('/sd'))


# Display GPy version
# print('version ', os.uname())

li = LIS2HH12(py)  #Accelerometer
# print("Acceleration: " + str(li.acceleration()))
# print("Roll: " + str(li.roll()))
# print("Pitch: " + str(li.pitch()))


# configure the MQTT client

aws = config["aws"]
pycomAwsMQTTClient = AWSIoTPythonSDK.MQTTLib.AWSIoTMQTTClient(aws["CLIENT_ID"])
pycomAwsMQTTClient.configureEndpoint(aws["AWS_HOST"], int(aws["AWS_PORT"]))
pycomAwsMQTTClient.configureCredentials((aws["AWS_ROOT_CA"]), aws["AWS_PRIVATE_KEY"], aws["AWS_CLIENT_CERT"])

pycomAwsMQTTClient.configureOfflinePublishQueueing(int(aws["OFFLINE_QUEUE_SIZE"]))
pycomAwsMQTTClient.configureDrainingFrequency(int(aws["DRAINING_FREQ"]))
pycomAwsMQTTClient.configureConnectDisconnectTimeout(int(aws["CONN_DISCONN_TIMEOUT"]))
pycomAwsMQTTClient.configureMQTTOperationTimeout(int(aws["MQTT_OPER_TIMEOUT"]))
pycomAwsMQTTClient.configureLastWill(aws["LAST_WILL_TOPIC"],aws["LAST_WILL_MSG"], 1)

#Connect to MQTT Host
if pycomAwsMQTTClient.connect():
    print('AWS connection succeeded')

# Subscribe to topic

pycomAwsMQTTClient.subscribe(aws["TOPIC"], 1, Data.customCallback)
time.sleep(2)

# Init CAN (pin order is Tx, Rx)
can = CAN(mode=CAN.NORMAL, baudrate=500000, frame_format=CAN.FORMAT_EXT, pins=('P19', 'P13'))
#can = CAN(mode=CAN.NORMAL, baudrate=500000, frame_format=CAN.FORMAT_STD, pins=('P19', 'P13'))
can.callback(handler=Data.can_callback, trigger=CAN.RX_FRAME)
#can.send(id=12, data=bytes([1, 2, 3, 4, 5, 6, 7, 8]))

while (True):
    coord = l76.coordinates()
    date  = rtc.now()

    accel = li.acceleration()
    roll  = li.roll()
    pitch = li.pitch()
    batt  = py.read_battery_voltage()

    
    #Save data in SD Card

    fw = open('/sd/gps-record.txt', 'a')
    fw.write("Coord {}, date {}, Accel : {}, Roulis {}, Tangage {}, Tension {} V\n".format(coord, date, accel, roll, pitch, batt))
    fw.close()
    #print("{} - {} - {}".format(coord, rtc.now(), gc.mem_free()))
    #print("Coord {}, date {}, Accel : {}, Roulis {}, Tangage {}, Tension {} V".format(coord, date, accel, roll, pitch, batt))
    #data.send(pitch, roll, coord)
    

    #Data format Json

    v0       =   "{"
    roll1    =   "\"roulis\":\""
    pitch1   =   "\", \"tangage\":\""
    date1    =   "\", \"date\":\""
    coord2   =   "\", \"coordonnes\":\""
    accel1   =   "\", \"acceleration32\":\""
    v1       =   "\"}"
    payloadmsg      = ('{}{}{}{}{}{}{}{}{}{}{}{}'.format(v0, roll1, roll, pitch1, pitch, date1, date, coord2, coord, accel1, accel, v1))
    payloadmsg1     = json.dumps(payloadmsg)
    payloadmsg_json = json.loads(payloadmsg1)

    #message1= '{"roulis":'+'"'+str(roll)+'",'+'"tangage":'+'"'+str(pitch)+'",'+'"acceleration":'+'"'+str(accel)+'"}'


    # Publish MQQT 
    pycomAwsMQTTClient.publish(aws["TOPIC"],payloadmsg_json,1)
    time.sleep(4)